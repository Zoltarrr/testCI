from django.apps import AppConfig


class HelloConfig(AppConfig):
    name = 'testCI.apps.hello'
