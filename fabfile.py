from __future__ import with_statement
from fabric.api import *
from contextlib import contextmanager as _contextmanager


env.forward_agent = True
env.user = "root"
env.roledefs = {'test_ci': ['159.203.166.224']}


@_contextmanager
def deploy_env():
    projects_dir = '/opt/shabbosmode_ci/testCI/'
    env_activate = 'source /opt/shabbosmode_ci/test_env/bin/activate'
    with cd(projects_dir):
        with prefix(env_activate):
            yield


@roles('test_ci')
def test_ci():
    with deploy_env():
        run("git reset --hard")
        run("git pull origin master")
        run("pip install -r requirements.txt")